package es.palacios.controller;

import es.palacios.Formulario;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class ResultadoController implements Initializable {

    @FXML
    public Pane raiz;

    @FXML
    private Label lbNombre, lbApellidos, lbComentario, lbCiudad, lbGenero, lbSo, lbHoras, lbFecha;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {

    }

    @FXML
    public void recuperarDatos() {
        Stage stage = (Stage) this.raiz.getScene().getWindow();
        Formulario formulario = (Formulario) stage.getUserData();

        lbNombre.setText(String.format("Nombre ==> %s",formulario.getNombre()));
        lbApellidos.setText(String.format("Apellidos ==> %s",formulario.getApellidos()));
        lbComentario.setText(String.format("Comentario ==> %s",formulario.getComentario()));
        lbGenero.setText(String.format("Genero ==> %s",formulario.getGenero()));
        lbCiudad.setText(String.format("Ciudad ==> %s",formulario.getCiudad()));
        lbSo.setText(String.format("S.O ==> %s",formulario.getSo()));
        lbHoras.setText(String.format("Horas ==> %s-3",formulario.getHoras()));
        lbFecha.setText(String.format("Fecha ==> %s",formulario.getFecha()));
    }

}
