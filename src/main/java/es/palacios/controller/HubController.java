package es.palacios.controller;

import es.palacios.CalculadoraApp;
import es.palacios.FormularioApp;
import es.palacios.LoginApp;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class HubController {

    @FXML
    protected void onButton1Click() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(LoginApp.class.getResource("Login.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Ejercicio 1 - Login");
            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    protected void onButton2Click() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(CalculadoraApp.class.getResource("calculadora.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Ejercicio 2 - Calculadora");
            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @FXML
    protected void onButton3Click() {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(FormularioApp.class.getResource("formulario.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            Stage stage = new Stage();
            stage.setTitle("Ejercicio 3 - Formulario");
            stage.setScene(new Scene(root));
            stage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
