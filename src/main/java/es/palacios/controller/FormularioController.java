package es.palacios.controller;

import es.palacios.Formulario;
import es.palacios.FormularioApp;
import es.palacios.ResultadoApp;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.io.IOException;

public class FormularioController {

    @FXML
    private TextField textfield1;

    @FXML
    private TextField textfield2;

    @FXML
    private TextArea textarea;

    @FXML
    private RadioButton hombre;

    @FXML
    private RadioButton mujer;

    @FXML
    private RadioButton privado;

    @FXML
    private ChoiceBox ciudades;

    @FXML
    private ChoiceBox sos;

    @FXML
    private Slider horas;

    @FXML
    private DatePicker fecha;


    @FXML
    protected void onEnviarButtonClick(ActionEvent event) {
        String nombre = textfield1.getText().trim();
        String apellidos = textfield2.getText();
        String comentario = textarea.getText();
        String genero = null;
        if (hombre.isSelected()) {
            genero = "hombre";
        } else if(mujer.isSelected()) {
            genero = "mujer";
        } else if(privado.isSelected()){
            genero = "privado";
        }
        String ciudad = ciudades.getValue().toString();
        String so = sos.getValue().toString();
        String hora = String.valueOf(horas.getValue());
        String fech = fecha.getValue().toString();

        Formulario formulario = new Formulario();
        formulario.setNombre(nombre);
        formulario.setApellidos(apellidos);
        formulario.setComentario(comentario);
        formulario.setGenero(genero);
        formulario.setCiudad(ciudad);
        formulario.setSo(so);
        formulario.setHoras(hora);
        formulario.setFecha(fech);

        Node node = (Node) event.getSource();
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(ResultadoApp.class.getResource("resultados.fxml"));
            Parent root = (Parent) fxmlLoader.load();
            stage.setUserData(formulario);
            Scene scene = new Scene(root);
            stage.setTitle("Resultados");
            stage.setScene(scene);
            stage.show();
        } catch (IOException e) {
            System.err.println(String.format("Error creando ventana: %s", e.getMessage()));
        }
    }
}
