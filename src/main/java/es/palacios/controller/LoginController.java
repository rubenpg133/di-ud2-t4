package es.palacios.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;

public class LoginController {
    @FXML
    private Label welcomeText;

    @FXML
    private TextField usuario;

    @FXML
    private PasswordField contraseña;

    @FXML
    protected void onEnviarButtonClick() {
        String usuarioText = usuario.getText();
        String contraseñaText = contraseña.getText();

        if (usuarioText.equalsIgnoreCase("batoi") && contraseñaText.equalsIgnoreCase("1234")) {
            welcomeText.setText("Login Correcto!");
            welcomeText.setTextFill(Color.GREEN);
        } else {
            welcomeText.setText("Login Incorrecto!");
            welcomeText.setTextFill(Color.RED);
        }

    }
}
